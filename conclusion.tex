\chapter{Conclusion}\label{chap:conclusion}

There is an irony in considering long--lost oral practices through the artefacts
that they leave behind, and in doing so using written sources to reveal an
unwritten activity. It impacts how we approach, think about, and analyse the
music contained within, using literate apparatus upon music that had a chance
collision with the written page, perhaps a glancing blow that sent it spinning
off in a new direction. However, these artefacts are all that remains of
a living tradition, and we must be keenly aware of our own assumptions when
approaching them. \textcite[443]{page10} writes that for a medieval singer,
a `melody principally existed as a feeling in the throat, a sensation in the ear
and a penumbra of associations created by years of repetition', and this
captures how notationally illiterate but musically virtuosic the vast majority
of medieval singers must have been within their own traditions. Full
reconstruction of a tradition is, of course, impossible as the music has been
irreparably transformed in its writing--down.\par

What little we can discover about the possibility of an oral tradition can be
found through detailed analysis of the artefact as well as its contents. Further
to the usual concerns of palaeography, the codicology of the MS can inform as to
the production, purpose and the history of the artefact after its writing. We
can see that, contrary to \textcite{roesner74}, \Wone{} is not a MS that saw
daily use, but neither was it preserved as a venerated monument of polyphony. It
served a practical use, but was never used in performance. The most likely
situation therefore, is that \Wone{} was a repertory--repository of polyphony
for St~Andrews, and served as a teaching aid and \textit{aide memoire} for those
that performed the ND repertory from memory.\par

The medieval mind, when trained for the memorisation of vast quantities of
information, had a skill for memory that far outstrips the common skill for
memorising information such as words and music that we find today, and this has
caused many writers to disbelieve any evidence that implies oral transmission,
instead considering the defects of a literate transmission just as Horace
apologises for Homer's supposed faults. However, oral transmission is not as
simple as memorising music note--by--note, and can take three forms:
memorisation, gist construction, and oral composition. None are particularly
concerned with precision of transmission, but focus on the ideas behind the
work. Smaller and larger changes may occur through each of the oral transmission
processes, but this matters little without the idea of an ``original'' to
deviate from.\par

Writing down an oral item changes and literalises it. In telling oral stories,
all three processes of oral transmission are at work, and complement each other
for an effective transmission. MSS such as VT demonstrate how the ND repertory
may have been an orally--composed repertory throughout its existence, without
reliance on writing. Each witness of ND polyphony in MS can only be a window
with a view into one of many living practices, and never approach a true
practice. Each window has a slightly different perspective on the repertory,
transforming the music into local performance practices by virtue of geography,
culture and local musical idioms.\par

Future work will be able to use the generated comparative edition from this
study to find more divergences between the sources of the ND repertory that can
provide evidence for oral processes at play. Rather than attempt to normalise
the differences between the sources so that an \textit{Urtext} can be found,
future work should bring these differences to the foreground, and this study
provides a methodology and tools for undertaking this. Further to the relatively
small differences that have been studied here, it is possible that larger
differences in the make-up of clausulae can also be studied using similar
methods.\par

The music and notation of \Wone{} demonstrate a ``residual orality'', and this
can be seen in the many levels of divergence between \Wone{} and the other
sources of ND polyphony. Some differences, such as systematic re-ligation or
errors in copying can easily be explained by the usual solutions of scribal
error and idiomatic editing. However, this leaves many differences that cannot
be explained away with literate processes, and this demonstrates that the
transmission of ND polyphony to St~Andrews must involve products of an oral
transmission. Among those differences that \Wone{} in particular exhibits, the
constant small alterations in ligature configuration and pitch are not merely
`background variation'\footnote{A useful term used to describe these phenomena
in seventeenth--century music, see \textcite[97]{howard12}.} within a fully
stable and literate repertory, but the result of respelling and re-notating the
same music with subtle solutions.\par

Larger differences, too, can be evidence for oral transmission. Small
differences in ligature groupings can result in large differences in implied
rhythm, and where a complex rhythm may fall outside the usual bounds of modal
rhythm, diverging solutions can be found in each MS\@. It is clear also that
oral composition plays a part in those sections of polyphony that are only
roughly concordant with one another, and common oral formulae demonstrate an
interplay between a remembered polyphony and its manifestation. Finally,
divergent clausulae demonstrate oral transmission of a purely thematic variety,
a powerful counterpoint to the idea of original form or original composition.
\Wone{} demonstrates that the ND repertory was never a completely stable and
finished set of compositions but, from its emergence to its eventual
writing--down, ND polyphony consisted of a fluid repertory full of change and
renewal.
