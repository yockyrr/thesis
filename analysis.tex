\chapter{Formulae Analysis}\label{chap:analysis}

``Residual orality'' may be present in \Wone{}, just as it may be present in any
other medieval source. Through careful analysis of the construction of the
musical text within the MS in relation to other sources of ND polyphony, it may
be possible to shed some light on the aspects of orality in the ND repertory
contained in \Wone{}\@.\par

\Wone{} contains very few \textit{unicae}: nearly every setting in the MS
appears elsewhere in other sources from across Europe, and many are present in
at least two other MSS, usually \F{} and \Wtwo{}\@. In a few cases, there are
concordances within \Wone{} itself, either between clausulae or indeed similar
organum settings. In general for this study, this means that for much of
\Wone{}, it is not the only ``copy'' of the music in existence. We can usually
find a version of the same music reproduced in some form or another elsewhere.
This affords ample opportunity within the MS to compare in minute detail these
written down versions of each setting, to document how they differ, and analyse
in detail how these differences can provide evidence for oral transmission. As
seen in previous chapters, oral transmission can take place in three forms: rote
memorisation (memory for words), gist memorisation (memory for things), and oral
composition.\par

It is important to note that these three forms are only partially distinct:
rather than three alternate methods that are selected to the exclusion of
others, these processes are \textit{aide memoire} tools that complement each
other. A gist memorisation may be aided in part by aspects of rote memorisation,
and a misremembered rote memorisation can be aided by gist memorisation and oral
composition. This results in a complex process of memorisation which is
difficult to untangle, and it is likely never to be known exactly how a certain
passage was remembered because it may well have never been memorised in exactly
the same way for each transmission. \textcite[183]{lord95a} prefers to maintain
a useful distinction between what a singer has consciously memorised and what is
simply remembered by `a natural and informal mental process', especially when
there is no fixed text to memorise. The same could be said of \Wone{}, that it
does not contain solely memorised, literate music nor does it contain only
improvised, oral--composed music. \Wone{} is rather the artefact of
a transmission that may be half--memorised, half--remembered.\par

It is also important to note that no method can guarantee, or even is much
concerned with, precise transmission. These oral transmissions are more focused
upon the act of transmission itself in performance, and transmitting the ideas
contained in the item. The orally--transmitted item is never composed in full
and therefore never reaches a completed state, but it is continuously being
altered and renewed in each of its subsequent transmissions.\par

The first method to consider is rote memorisation. Music is memorised
practically verbatim this way through a process of continuous repetition, and is
recalled serially such that the item must be recalled from the beginning. Useful
here is the etymology of the medieval Latin word ``verbatim'' (word for word).
This is in contrast to the term ``literatim'' (letter for letter). Verbatim
memorisation, unlike literatim, does not imply a written intermediary, as
memorising speech is memorising the words that are said. The letters that are
spoken, if we can say that ``speaking letters'' makes sense at all, is a
literate construction that occurs only in the use of writing.\par

Academic writing that concerns the ``defects'' of oral transmission is usually
referring to the verbatim memorisation method alone. Such ``defects'' can be
found in the superficial changes between versions, for example alternative
spellings and slight, inconsequential changes that do not alter or interrupt the
broad flow of the item. Both large-- and small--scale structure is likely to be
identical in a memorised transmission, but minute changes can be seen, typically
from a legitimate error in recall. Within the context of an oral repertory
however, it is not fair to call such superficial changes ``defects'' as they are
a wholly expected and perhaps desired feature of the items' transmission.\par

In musical terms for the ND repertory, these superficial changes would be
manifest in settings that give themselves easily to rote memorisation, such as
discant tripla and quadrupla, where the synchronicity of voices is key, modal
rhythm is continuous, and there is no room for a voice to add or remove a phrase
either by error or improvisation. We know that for words, rhythmic items are
more conducive to rote memorisation, as can be seen in versified
treatises~\autocite[98--102]{busseberger05}, and this must be the case for music
too, where a tightly regularised and rhythmic music ensures that nothing can be
left out or altered. We can therefore expect the construction and individual
phrases of pieces memorised by rote to be broadly identical in strict rhythm,
but the written--down versions still to exhibit small, superficial changes in
pitch and rhythm. Moreover, if each written--down version proceeds from an oral
recall of the musical item, then we can expect the ambiguous ND musical notation
to reflect this: different clefs, layout and ligation is to be expected as the
scribes rewrite the music, much like attempting to spell a word that you have
only heard spoken before.\par

The second method, gist memorisation, is perhaps a more interesting process to
analyse. Music is memorised this way through a combination of rote and gist. The
item fits into and is known within an enculturation framework where an item is
transformed from its manifestation (in this case a performance of the music)
into a gist and plan of the item. In musical terms, the item is not known as
a series of pitches, but as a route through a well--known map of the musical
territory. Phrases are generalised and categorised into a general knowledge of
common formulae, this being a more efficient method of remembering an entire
corpus of music which builds upon the same formulae. Rather than committing
a phrase to memory each time it occurs, the phrase is memorised once and tagged
in memory. A performer, fully immersed in the culture of performing a repertory
such as this, will have built up a bank of oral formulae, such that a piece of
music is a memorised plan of how to navigate polyphony using these stock
phrases.\par

We may however, find moments that become more like rote memorisation, either
because they are especially rhythmic and therefore memorable, or because they do
not fit neatly into the typical framework of oral formulae and must be added as
a new formula for that particular instance. In ND repertory terms, this may be
observed in differences that go beyond common superficial changes yet accomplish
the same musical function. Oral formulae may be selected that are broadly
similar and achieve the same aim --- such as from one harmonic concord to
another --- but may be implemented in a different way. A simpler formula may be
replaced by a more elaborate one or vice versa. A complex clausula may be
replaced by a shorter paraphrase or vice versa. Even a completely different
section of polyphony may be substituted, but crucially one that begins and ends
at the same sense of place. These are not large--scale changes, but quick
substitutions that have little overall effect on the musical flow.\par

The final method is that of oral composition. Here, all the previous processes
of oral formulae still apply, but the gist becomes a much more fluid
construction, transformable just as much as the individual notes. In ND terms,
we would expect to see large--scale changes to the settings, whole new clausulae
substituted in place, and settings that completely diverge from one another,
becoming more like improvisations upon a theme rather than those settings that
have been seen previously as examples of static and composed works. Sometimes,
these alternate versions may appear in other clausulae cycles, but at other
times these alternate sections of polyphony may be unique within the entire
repertory.\par

In order to detect these elements of residual orality which may be present in
\Wone{}, an analysis of the overlapping settings of polyphony can be made, and
the divergences between concordant settings may provide evidence for oral
transmission by any of the processes described above, most likely through
a combination of all three. By detailing differences, large and small, between
settings that occur both in \Wone{} and other MSS, a comparative edition of
\Wone{} can be made that aims not to hide those differences behind
a comprehensive editorial process, but instead bring them to the forefront of
discussion. To my knowledge, there have been only two studies of the ND
repertory that consider these differences between the MSS of the repertory as an
important aspect of their transmission: \textcite{smith64} and
\textcite{tischler88}.

\textcite{smith64} is concerned with cataloguing and defining clausulae, and as
such divides each setting of organum into a conception of their constituent
clausulae, theorising the broad delineation of those clausulae that may replace
sections or organum by virtue of their tenors.  \citeauthor{smith64} supplements
his study with his findings in diagrammatic form, and fully shows those
larger--scale differences between the versions contained in MSS\@.\footnote{An
example pertaining to this study can be found in \autoref{fig:smith-m12}.}
Rather than the cataloguing and organisation of organa by setting,
\citeauthor{smith64} conceives of the construction of the organa as `essentially
and overtly sectional'~\autocite[20]{smith64}, and the ND organa as compositions
of clausulae. However, \citeauthor{smith64} does not go much further than to
find fault with the idea of organa as self--contained compositions.\par

\textcite{tischler88} deals with these same issues of clausulae, but from a
finer scale, taking as read the sectional construction of two--part organa and
using \textcite{ludwig70} and \textcite{smith64}'s clausulae demarcations to
create an entire edition of the two--part organa, demonstrating their
differences in clausulae, pitch and ligation. \citeauthor{tischler88} endeavours
to edit the original ligatures into modern notation, writing out every clausula
rhythmically. However, small and seemingly inconsequential differences to
\citeauthor{tischler88}'s study, such as slight changes in ligation that do not
alter the rhythm, are ignored in order to maintain the integrity of the clausula
as an independent section of polyphony.\par

This study intends to take an even finer--grained look at the differences
between the organa, concentrating on \Wone{} as a basis for study, considering
all manner of differences between MSS as possible evidence for oral
transmission. Taking after \textcite{tischler88}, this study will display those
differences in the form of an edition, displaying the music in \Wone{} as a base
text upon which divergences in other sources occur, but divergences can take any
form: large differences in clausula to small changes in pitch and ligation are
considered equally.\par

Using \textcite{ludwig70}'s concordances as a basic indication of concordance,
\Wone{} contains 198 settings of organa that have some form of concordance in
other MSS\@. To calculate this figure, only concordances in \textit{sine
littera} notation have been considered, as \textit{cum littera} concordances ---
for example in motet --- are bound to be transformative, and their lack of
ligation complicates the question of the indication of rhythm. Furthermore,
divisiones are not always shown as they cannot be reliably placed in the text
and an attempt at concordance with divisiones would needlessly clutter the
result without much more information being displayed.\par

The edition was compiled using a shorthand markup language of my own creation
called NDP (the Notre Dame Parser), which facilitated rapid entry of the
differences between the sources. NDP is a file format that can concisely encode
the pitch, text, ligation, and divergences between sources of the ND repertory.
It is also a parser made using the common tools Lex and Yacc which define
a grammar to read an NDP file and output a more verbose Lilypond typesetting
code which can then be piped into Lilypond to generate editions in PDF\@.
An explanatory example of NDP can be found in \autoref{fig:ndpfile}, which
demonstrates how most symbolic events can be represented using a single
character, aiding concise and thereby rapid entry of the necessary data.\par

\begin{figure}[!tp]
	\begin{lstlisting}
#         // Begin source description
a Adam    // Source with identifier "a" has name "Adam"
e Eve     // Source with identifier "e" has name "Eve"
#         // End source description
{         // New ordo
 {        // Begin voice one
  !''     // C clef, second line
  [       // Begin square ligature
   g'     // Note g (jump up an octave)
   a      // Note a
  ]       // End square ligature
  a       // Note a
  |       // Divisione
 }        // End voice one
 {        // Begin voice two
  !'''    // C clef, third line
  a       // Note a
  |       // Divisione
 }        // End voice two
}         // End ordo
{         // New ordo
 {        // Begin voice one
  /"a"    // Source "Adam" has a divergence
  (       // New currentes ligature
   edc    // Notes c,d,e
  )       // End currentes ligature
  /"e"    // Source "Eve" does something different
  [       // New square ligature
   efe    // Notes e,f,e
  ]       // End square ligature
  /       // End divergences
  |       // Divisione
 }        // End voice one
 {        // Begin voice two
  a       // Note a
 }        // End voice two
}         // End ordo
$         // Begin syllable description
Con2      // Syllable "Con" has a duration of two ordines
$         // End syllable description
	\end{lstlisting}
	\caption{An example NDP file}\label{fig:ndpfile}
\end{figure}

The musical text of \Wone{} was entered first, and then this base entry was
compared against the concordances listed in \textcite{ludwig70} one--by--one,
noting any difference as it arose, until a complete NDP file for that setting
was created. This NDP markup was then compiled into a written--out edition. The
complete edition can be found in \autoref{appendix:edition}\@. This full edition
of \Wone{}'s concordances and, more importantly, divergences provides evidence
in the slight differences between MSS that can point to oral transmission. These
will be discussed, using examples from the full edition, and how these
differences pertain to oral transmission will be highlighted.\par

By far the largest category of differences between each of the sources is those
slight differences in ligation. There is not a single concordant setting that is
identical in the presentation of its ligature configuration. A simple parallel
can be found in the different spellings found within copies of medieval texts
indicating an oral component playing a part in the copying of texts. A different
spelling indicates that the word was not copied literatim, but verbatim. The
word was read (decoded) and recognised before being re-encoded into a different
and perhaps idiomatic spelling. Many of these ligature ``spellings'' are
metaphorical homophones: they indicate an identical rhythm. These will not be
discussed in depth as they can be seen from a cursory inspection of any page of
the full edition, but it is worth mentioning that such alterations are
commonplace and are in themselves good evidence for an oral transmission. Other
changes in ligation alter the rhythmic interpretation, and it is these changes
that will be discussed below.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{split-01}
		\caption{4--note ligature is equivalent to two 2--note ligatures}\label{fig:foureqtwotwo}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{split-03}
		\caption{5--note ligature is equivalent to a 3-- plus 2--note ligature}\label{fig:fiveeqthreetwo}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{split-02}
		\caption{4--note ligature is equivalent to a puncta plus a 3--note ligature}\label{fig:foureqonethree}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{split-04}
		\caption{\Wone{} splits ligatures}\label{fig:wonesplitsligatures}
	\end{subfigure}
	\caption{Split ligatures}\label{fig:splitligatures}
\end{figure}

In \Wone{}, ligatures can be split and joined at will, as can be seen in
\autoref{fig:splitligatures}. Modal rhythm operates using the grouping of
ligatures to imply rhythm, such that this grouping is fundamental to the
interpretation of rhythm in ND sources. It has long been known that the
``rules'' of modal rhythm as described by Anonymous IV cannot be strictly
applied either to discant, copula or organum purum. \textcite[44]{bull17}
rightly notes that we cannot be certain of \Wone{}'s indication of modal rhythm,
considering that modern dating of the MS places the systematisation of modal
rhythm `at least ten years, and possibly twenty, after the creation of \Wone{}'.
Modal rhythm rules then, are more usefully interpreted as rules of thumb, and an
open--minded interpretation is sometimes necessary to translate these groupings
into a sane, synchronous rhythm, especially when such rhythms are irregular.
Broadly speaking for the purposes of this study, it will be assumed that the
ligation of \Wone{} follows the later systematisation where possible.
Notwithstanding, it is surprising to see ligatures longer than ternaria split
and joined seemingly at will.\par

In \autoref{fig:foureqtwotwo}, two 2--note ligatures, typically indicating
\inlinemus{BLBL}\ are written in \Wone{} as a single 4--note ligature, making
its modal interpretation more difficult. It is clear that the rhythm implied is
still the same, but this joining of ligatures complicates matters. The same is
true in \autoref{fig:fiveeqthreetwo}, where a 3--2 complex of ligatures is
joined into a single 5--note ligature. However, this rather simple joining of
ligatures is further complicated in \autoref{fig:foureqonethree} where the
prepending of a punctum into a 4--note ligature changes the implied rhythm,
from \inlinemus{LBLLL}\ into \inlinemus{BBBLL}. Although it may seem therefore
that the scribes of \Wone{} preferred to join ligatures where possible,
\autoref{fig:wonesplitsligatures} demonstrates that this is not always the
case, and that there are examples where ligatures joined in other sources are
not joined in \Wone{}\@.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{equivalence-01}
		\caption{At cadence points}\label{fig:binarialigated}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{equivalence-02}
		\caption{In mode V}\label{fig:binariamodev}
	\end{subfigure}
	\caption{Equivalence of binariae}
\end{figure}

Binariae most often indicate an iamb in modal rhythm. Two longas at a cadence
point are usually unligated, but the scribes of \Wone{} had a habit of ligating
these longas together, which confuses any interpretation of the rhythm, as seen
in \autoref{fig:binarialigated}. This is not just limited to cadence points, and
sometimes longas are ligated in mode V (see \autoref{fig:binariamodev}),
indicating a rhythm more complex than simplex longas.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{quadcurr-01}
		\caption{Aligned notation of \Wone{}, \Wtwo{} and \F{}}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{.3\textwidth}
		\centering
		\includegraphics[height=1.5cm,keepaspectratio]{F138v}
		\caption{As notated in \F{}, f.138v}\label{fig:f138v}
	\end{subfigure}
	\begin{subfigure}{.3\textwidth}
		\centering
		\includegraphics[height=1.5cm,keepaspectratio]{W146}
		\caption{As notated in \Wone{}, f.46}\label{fig:w146}
	\end{subfigure}
	\begin{subfigure}{.3\textwidth}
		\centering
		\includegraphics[height=1.5cm,keepaspectratio]{W282v}
		\caption{As notated in \Wtwo{}, f.82v}\label{fig:w282v}
	\end{subfigure}
	\caption{Equivalence of ligatures and currentes}\label{fig:ligeqcurr}
\end{figure}

There are two types of ligatures: square ligatures and currentes, and we can be
certain in \Wone{} that these ligature forms are equivalent. There is not a more
convincing example of this equivalence than \autoref{fig:ligeqcurr}, where the
order of square ligatures and currentes is switched between \Wone{}, \Wtwo{} and
\F{}\@. The form of ligature written is therefore nothing more than a stylistic
choice on the part of the scribe. Descending ternaria like in this example are
altered between square ligatures and currentes interchangeably, but it seems
apparent that stepwise descending ligatures of more than three notes are the
most likely candidates to be written as currentes.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{plica-01}
		\caption{Single written--out plica in \Wone{}}\label{fig:writtenoutplica}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{plica-02}
		\caption{A plica complex}\label{fig:plicacomplex}
	\end{subfigure}
	\caption{Written--out plicae}\label{fig:writtenoutplicae}
\end{figure}

Plicae are notes that are not explicitly written, but implied by the position
of a stem at the end of the ligature. We can be certain, too, that plicae are
equivalent to their written--out forms, and are simply a scribal preference,
not any kind of extraordinary note.  \autoref{fig:writtenoutplicae}
demonstrates this: in \autoref{fig:writtenoutplica}, a \textit{g} written as
a plica in \F{} is instead written out in \Wone{}\@. Even more convincingly,
\autoref{fig:plicacomplex} presents a plica complex, where through re-ligation
different notes have been chosen to become plicae. The purpose of plicae it
seems in this case is not as a scribal shorthand like currentes may be, but they
are in fact a way of better indicating the intended rhythm, especially when
\textit{fractio modi} is involved: the plica is a method of ``tucking in'' an
extra, usually stepwise, note at the end of a longa without disturbing the
regular grouping of ligatures and complicating the interpretation of rhythm. For
example, in \autoref{fig:writtenoutplica}, the use of a plica in \F{} better
demonstrates that the intended rhythm is \inlinemus{BBB}, not \inlinemus{BLL},
a possible incorrect interpretation of \Wone{} or \Wtwo{}\@.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{rhythm-01}
		\caption{\Wone{} substituting rhythm}\label{fig:wonesubstitutingrhythm}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{rhythm-02}
		\caption{Substituted rhythm in other MSS}\label{fig:substitutedinotherMSS}
	\end{subfigure}
	\caption{Substituted rhythm}\label{fig:substitutedrhythm}
\end{figure}

Repeated notes are often substitued both in \Wone{} and in other MSS\@. The
common binaria at cadences, often interpreted as two simplex or duplex longas
(\inlinemus{LL} or \inlinemus{DLDL}), can be subsituted for a binaria followed
by a puncta, i.e\@. \inlinemus{LBL} or \inlinemus{LLDL}. This can be seen in
\autoref{fig:substitutedrhythm}, where in \autoref{fig:wonesubstitutingrhythm},
the move from a lower auxiliary \textit{G} to \textit{A} is emphasised in
\Wone{} with a repetition of the \textit{A}, and again at the end of the
extract. Another emphasis occurs in \autoref{fig:substitutedinotherMSS} where Ma
does not emphasise at all, \Wone{} and \F{} emphasise all but the first cadence
of the extract, and LoA chooses to emphasise each cadence.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{difficult-01}
		\caption{Differently--ligated ordo}\label{fig:differentlyligatedordo}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{difficult-02}
		\caption{Systematic re-ligation}\label{fig:systematicreligation}
	\end{subfigure}
	\caption{Difficult--to--notate ordines}\label{fig:difficulttonotateordines}
\end{figure}

\begin{figure}[!tp]
	\centering
	\includegraphics{difficult-03}
	\caption{Difficult--to--notate clausula}\label{fig:difficulttonotateclausula}
\end{figure}

Another divergence occurs in those ordines that are ligated differently in
every source. \autoref{fig:differentlyligatedordo} demonstrates four different
ways in which one ordo has been ligated in four MSS, including \Wone{}\@. It is
difficult to determine what has occurred here: perhaps the performance practices
that fostered each MS had a different conception of the rhythm of this ordo, the
`house style' that \textcite[261]{roesner01} hinted at. Alternatively, the
intended ``original'' rhythm of this ordo may have been either too complicated
to be precisely notated in modal rhythm, leading to the skein of interpretations
presented here, or this ordo is without measure, in organum purum. Taken as it
is from a discant quadruplum, this last possibility seems unlikely as the
interpretation must fit perfectly within four longas. It is my opinion that the
answer probably lies somewhere between the first two possibilities: each
performance practice had their own idiomatic way of performing, and this was
complicated when those idioms fell outside the usual boundaries of modal
notation.\par

\autoref{fig:systematicreligation} provides more evidence for the possibility of
house styles playing a part, and such examples are easy to find, listed in
detail by \textcite{ludwig70}. Here the pitches are identical, but have been
systematically re-ligated: \Wone{} in mode I, \F{} in mode II\@. This example
cannot be a simple ``difficult rhythm'' issue, as both ligations are fine models
of their rhythmic mode, so there must have been an intended alteration at some
point. Many authors point towards a scribal practice of systematically
re-ligating clausulae such as this, but there is no evidence to suggest that
this process must have taken place at the moment of writing, and may have in
fact been part of how that clausula was already performed in that practice.\par

These diverse styles of performing what is essentially the same polyphony are
exemplified in \autoref{fig:difficulttonotateclausula}, where a confluence of
systematic re-ligation and confusing ligature configurations have resulted in
three different, yet unmistakably similar interpretations of the same music.
\Wone{}'s interpretation is a systematic re-ligation of \Wtwo{} and \F{}'s
interpretation, but even between \Wtwo{} and \F{}, there are slight differences
that occasionally alter their interpretation.\par

\begin{figure}[!tp]
	\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{ornament-02}
		\caption{Ornamentation through plicae}\label{fig:plicaeornamentation}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{ornament-01}
		\caption{Ornamentation through extra notes}\label{fig:ornamentationtwiddle}
	\end{subfigure}
	\caption{Ornamented ligatures}\label{fig:orgnamentedligatures}
\end{figure}

Some ligatures are more elaborate than others, having been ornamented to
accommodate more notes. These may seem like small and superficial changes, but
are not alterations that could commonly arise through the usual ``defects''. In
\autoref{fig:plicaeornamentation}, we can see that whereas \Wtwo{} and \F{}
employ a plica between two binariae, \Wone{} uses three currentes to add a
passing \textit{D}, subtly ornamenting the same music in the same space of time.
In organum purum, ornamentation can be freer without the constrains of modal
rhythm, and divergences such as those in \autoref{fig:ornamentationtwiddle}
occur, where a relatively simple ternaria in \Wone{} is replaced with a 5-- and
6--note ligature in \F{} and \Wtwo{} respectively, building on the same
alternating \textit{Bc} figure.\par

\begin{figure}[!tp]
	\centering
	\includegraphics{thirdout-01}
	\caption{A ``third out'' error, shown here between $\dagger$\ symbols}\label{fig:thirdout}
\end{figure}

There is however, one kind of difference which must be the product of scribal
error. That is the ``third out'' type of error, occurring a few times within
\Wone{} (see example in \autoref{fig:thirdout}), where the music is copied two
steps higher or lower than intended on the wrong staff line. We know that this
must be an error and not some alternate way of performing because the organa
creates many more dissonances than is normal, stopping at thirds and sevenths
much more than the common fifths and octaves. One can see this mistake being
made easily by a scribe: the sources employ C and F clefs exclusively and these
can occur on any staff line.\footnote{As with all things, there is a notable
exception. \Wone{} uses a D clef once: f.69 III.} Therefore, eyeskip could
easily cause the scribe to write music beginning on the wrong staff line
without changing the clef and this error could continue for a whole line or
more. This does not imply that the scribe was copying from a written exemplar,
rather the opposite: that a written exemplar presumably written in the same
clefs as \Wone{} would surely alert the scribe sooner that he is copying on the
wrong line, as the music copied a third out often goes much too far above or
below the stave than is usual.\par

\begin{figure}[!tp]
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{different-01}
		\caption{Slight difference makes a different ordo}\label{fig:slightdifferences}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{different-02}
		\caption{Re-ligation plus a transformed formula}\label{fig:transformedformula}
	\end{subfigure}
	\caption{Different ordines}
\end{figure}
\begin{figure}[!tp]
	\ContinuedFloat\centering
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics{different-03}
		\caption{Ordines of rough concordance}\label{fig:roughconcordance}
	\end{subfigure}
	\caption[]{Different ordines}
\end{figure}

Rather than these small, sometimes insignificant differences, it is the
large--scale differences that begin to show true oral formulae at work. There
are many examples of this in \Wone{}, but a few will suffice. The line between
re-ligation and slight differences, and larger differences that can be termed
a different ordo entirely is not particularly distinct. For example, slight
alterations such as those in \autoref{fig:slightdifferences} can quickly alter
an ordo to a different rhythm and different length. It is tricky to say whether
this ordo is concordant or is an example of a slightly different formula being
selected.\par

What we can plainly see however are those ordines that have been transformed
and the formula altered to achieve a slightly different result.
\autoref{fig:transformedformula} demonstrates one example of this. \Wone{} and
\F{} are roughly the same, proceeding in groups of 4--note currentes.  \Wone{}
continues this formula all the way down to \textit{G}, but \F{} alters the
formula with a repeated \textit{B} for the final step, dispensing with the
repeated \textit{G} that \Wone{} employs. \Wtwo{} however, has re-ligated the
formula in mode I, and ends its ordo two longas previous on a \textit{c}: still
concordant with the tenor, but at a different interval. All three sources use
the same formula to achieve the same result, but they go about it in different
ways: \Wone{} descends simply using the formula, \F{} adds a slight twist at
the end, and \Wtwo{} redefines the formula in a new rhythm and halts it before
it gets to the \textit{G}, effectively paraphrasing what is a longer ordo in
the other MSS\@.\par

There are also entire sections of polyphony that are only roughly concordant
with one another. An example can be seen in \autoref{fig:roughconcordance} which
divides roughly into two versions: the version in \Wone{} and \F{} (f.147), and
the version in \Wtwo{} and \F{} (f.65). These two versions of the same polyphony
achieve many of the same aims, their ordines often beginning and ending on the
same pitches, and often rising and falling in similar patterns. However, the
notes and melodic figures that make up these polyphonies can be very different.
At times they are more elaborate, adding more passing and auxiliary notes, but
at other times the formula can be more utilitarian, proceeding from one
concordance to another without much recourse to ornamentation.\par

\begin{sidewaysfigure}[p]
	\centering
	\includegraphics[keepaspectratio,width=\linewidth,height=\dimexpr\textheight-2\baselineskip]{smith-m12}
	\caption{Diagram of \textit{Alleluya.~Adorabo} from
	\textcite[394]{smith64}}\label{fig:smith-m12} \end{sidewaysfigure}

Finally, there are those concordances that are much larger than any small
example, and these are the largest set of divergences after small and
insignificant alterations. Clausulae that construct the settings of organum are
often selected and variegated at will, such as can be seen in
\textit{Alleluya.~Adorabo} (p.\pageref{edition:041-alleluya}).
\textcite{smith64}'s diagram can help here (reproduced in
\autoref{fig:smith-m12}), as it details all the clausulae that he detects in the
construction of this setting. We can see here that \textit{Alleluya.~Adorabo}
is not a singular construction, but can be constructed out of clausulae. This
diagram shows, for example, the numerous clausulae that can be selected for the
setting of ``et confitebor''.\par

What \citeauthor{smith64}'s diagram does not show, and what we can see in the
edition in this study, is the intermittent concordance relationship \Wone{} has
with the other sources. \F{} is mostly concordant, but does not contain the same
polyphony for the setting of ``ad templum sanctum tuum''. It is once again
roughly concordant for the three short ordines of ``et confite-'', but then
returns to its other clausula. \Wtwo{} tells a similar story. It begins
similarly to \Wone{} and \F{} but quickly uses its own clausula until ``-lu-''
where it becomes roughly concordant with \Wone{} and \F{} again. However, it
soon returns to its own clausula five ordines later, not returning until the
final cauda. In the verse, \Wtwo{} is hardly concordant, returning only for an
odd ordo. It begins similarly, but then completely diverges into completely
different music.\par

This final example, for which there are many similar examples in \Wone{} and
throughout the repertory,\footnote{Many good examples can be found in \Wone{}'s
fascicle IV, ff.25--48 (edition
pp.\pageref{edition:027-uiderunt-omnes}--\pageref{edition:090-tamquam}).}
demonstrates true oral formulae and oral composition. For
\textit{Alleluya.~Adorabo} and similar examples, there appears to be a rough
plan, and the performances as seen in the various MSS --- for the written down
versions are performative --- maintain this integrity of gist. There must have
been myriad diverse ways of performing polyphony over this particular tenor, and
each one was selected and improvised upon in performance. We can assume too that
those singing the tenor were also aware of the polyphony and knew how to react
to that clausula, perhaps rhythmically transforming the tenor in colores for
discant clausulae.\par
