.PHONY: diff full reqs bibclean index scores clean coloursplit
all: coloursplit
diff: reqs
	git latexdiff --main main.tex --no-view -o main-diff.pdf -v --prepare \
		'touch wordcount.txt; rm -f flags.tex echo "\copyrighttrue" > flags.tex; latexmk -f -interaction=nonstopmode; rm -f flags.tex; make' \
		83a55fe45bbebc5b4a515d15d2df2cc39e73dc0	
coloursplit: full
	rm -f *_colorsplit.pdf *_bwsplit.pdf
	./pdfcolorsplit -m -v main.pdf
full: reqs 
	rm -f flags.tex
	echo "\copyrighttrue" > flags.tex
	latexmk
	mv main.pdf main-copyright.pdf
	rm -f flags.tex
	echo "\copyrightfalse" > flags.tex
	latexmk
	rm -f flags.tex
reqs: count bibclean index scores
count: $(wildcard *.tex)
	texcount -inc -incbib -brief -sum -nosub main.tex > wordcount-status.txt
	grep "File(s) total" wordcount-status.txt | cut -d":" -f1 |\
		sed ':a;s/\B[0-9]\{3\}\>/,&/;ta' > wordcount.txt
	cat wordcount-status.txt
bibclean: *.bib
	biber --tool -g biber-tool.conf --output_align --output_indent=2 --output_fieldcase=lower \
		bibliography.bib && mv bibliography_bibertool.bib \
		bibliography.bib
index:
	rm -f flags.tex
	echo "\copyrighttrue" > flags.tex
	pdflatex main
	rm -f flags.tex
	makeindex main.nlo -s nomencl.ist -o main.nls
scores: mus/gamut.pdf mus/immel-01.pdf mus/immel-03.pdf mus/immel-06.pdf mus/immel-08.pdf mus/split-01.pdf mus/split-02.pdf mus/split-03.pdf mus/split-04.pdf mus/equivalence-01.pdf mus/equivalence-02.pdf mus/quadcurr-01.pdf mus/plica-01.pdf mus/plica-02.pdf mus/rhythm-01.pdf mus/rhythm-02.pdf mus/difficult-01.pdf mus/difficult-02.pdf mus/difficult-03.pdf mus/ornament-01.pdf mus/ornament-02.pdf mus/thirdout-01.pdf mus/different-01.pdf mus/different-02.pdf mus/different-03.pdf mus/different-04.pdf mus/BLBL.pdf mus/LBLLL.pdf mus/BBBLL.pdf mus/BBB.pdf mus/BLL.pdf mus/LL.pdf mus/DLDL.pdf mus/LBL.pdf mus/LLDL.pdf
	make -C mus/
clean:
	git clean -fxde '.git-credentials'
