\chapter{Introduction}\label{chap:introduction}

\begin{quote}
	\ldots and yet I also feel aggrieved, whenever good Homer ``nods,'' but
	when a work is long, a drowsy mood may well creep over
	it.\footnote{Translation from \textcite[480--481]{fairclough26}: ``et
	idem / indignor quandoque bonus dormitat Homerus / verum operi longo fas
	est obrepere somnum''.}
\end{quote}

It is this passage, from Horace's \textit{Ars Poetica} (lines 358--360), that is
the origin of the well--known phrase ``even Homer nods''.\footnote{Although the
odd choice of the word ``nods'' in \citeauthor{fairclough26}'s translation is
attributable to \citeauthor{dryden70}'s 1677 essay
\textit{\citefield{dryden70}{title}}: `Horace acknowledges that honest Homer
nods sometimes: he is not equally awake in every line; but he leaves it also as
a standing measure for our judgements'~\autocite[134]{dryden70}.} It is
a proverb commonly taken to mean that even the greatest artists are fallible:
one cannot expect artistic perfection throughout each and every artwork. Indeed,
the \textit{Iliad} and the \textit{Odyssey} are not, in a Romantic conception,
perfect works --- the famous example of these supposed imperfections is Homer's
monotonous description of the ``wine dark'' sea --- but Horace apologises for
their perceived faults: to him the slight errors are inconsequential when seen
through the lens of the entire work. A long work such as the \textit{Iliad} or
the \textit{Odyssey} should not be criticised for its blemishes but praised for
its qualities.\par

However, as far back as the eighteenth century the authorship of these two great
poems to one man has been questioned, and modern scholarship now generally
believes that the Homeric epics were not the work of one great poet, but in fact
two fine examples of an Ancient Greek collection of orally--composed
poetry~\autocites[12]{graziosi16}{fowler04}. When Homer ``nods'' therefore, he
is not being inattentive, on the verge of sleep. Rather, he is not entirely
responsible for the work as we receive it. It is true that the Homeric epics are
repetitious, error--prone and variable, but it is not because Homer made
continuity errors. It is instead because the epics are not just Homer's work:
such ideas of consistency were not of great concern to the oral composers of the
Ancient Greek world.\par

The same environment of orally--composed works forming part of a common culture
of art has also been true for the vast majority of music made in human history.
There are few exceptions, the most notable being that of the previous few
centuries of the Western art music tradition, a tradition whose shadow still
looms large over much musicological scholarship and whose study still influences
the studies of music far removed from its usual apparatus, temporally as well as
geographically and culturally.\par

Just as we have longed to see Homer as a Romantic poet, composing epic works
from nothing and feeling disappointed when he does not live up to our
expectations as a modern--day poet, musicology has falsely and failingly
attempted to construct Beethovenian--myth figures in early, modern, folk,
traditional, and non-European music. We can see this force at work in a practice
commonly known as the Notre Dame school, a style of florid polyphonic singing
that emerged in the twelfth century and flourished across Europe in the
mid-thirteenth century.\par

The Notre Dame school is regularly held up as the true beginning of the Western
art music tradition: named composers transforming `polyphony from a performing
practice into ``composition'' in the modern sense'~\autocite{roesner01a}. Much
of this idea is due to the late--thirteenth--century writer Anonymous IV, who
attributed the entirety of the music of the Notre Dame school to two men:
\Leonin{} and \Perotin{}\@. \Leonin{} wrote a \MLO{} containing polyphony for
the Mass and Office, then \Perotin{} later edited and abbreviated this book,
adding more rhythmical compositions, or so the narrative goes. This rhythmical
discant led to motet, motet led to the \textit{ars nova}, which eventually led
to the music of the Renaissance, where we can begin to pick up the story with
more documentary evidence and lead to composers such as Bach. In this narrative,
which persisted for much of the twentieth century, \Leonin{} and \Perotin{} are
the first true ``composers'' of music. They transformed a supposedly inferior
practice of improvisation into a recognisably modern and superior compositional
practice. The common consensus until more recently was that the work of
\Leonin{} and \Perotin{} was the first step on the road to the venerated history
of Western art music, the first composers in the great men theory of
history.\par

However, more recent scholarship has called into question this interpretation of
the facts and the elevation of the names of \Leonin{} and \Perotin{} to
mythological status. Here, rather than two composers, we may in fact have two
Homers: men who may have had a large part to play in the construction of their
repertories, but whose names and importance have been inflated through time
until they became fully--fledged composers.\par

Within the last few decades, multiple avenues of musicology have begun to
question the role of composer in many kinds of music, most notably in music from
before the seventeenth century. Recent authors have focused upon the teaching of
counterpoint and how pedagogical treatises have been misinterpreted to draw
a non-existent distinction between historical improvisation and composition.
\textcite{wegman96} looks for example at Tinctoris' \textit{Liber de arte
contrapunti} and makes the important point that nowhere `does Tinctoris imply
that one must learn to devise correct successions of consonant intervals by
actually writing out examples such as he provides'~\autocite[432]{wegman96}.
\textcite{mcgee03} broadened this discourse, by documenting multiple examples
and processes for the employment of improvisation in varying quantities across
the arts until at least 1700. \textcite{morucci13} has looked in detail at
improvised vocal counterpoint in sixteenth--century Italian treatises and frames
this discussion as not merely a sixteenth--century phenomenon but a practice
that had been in continuation as `a long--standing didactic
tradition'~\autocite[2]{morucci13}. The interest in considering early music in
terms of its improvisatory content and processes is such that
\textcite[1]{guido17} opens this recent contribution in relation to Renaissance
and Baroque music by describing the field as `like a wide river that has been
accumulating momentum'. It is no surprise therefore that five further chapters
in this same book open with sentences validating the recent surge in scholarly
interest in historical improvisation.\par

Rejection of the idea of a modern composer role in early music and the
associated priorities of the \textit{Kunstwerk} and obedience to
\textit{Werktreue} has had a large impact on study of thirteenth--century music
and the use of the traditional names of \Leonin{} and \Perotin{} in connection
with the Notre Dame school, such that many writers prefer to substitute the
terms \textit{Leoninian} or \textit{Perotinian} (just as the works of Homer have
become the \textit{Homeric epics}), or indeed dispense with such names entirely
unless thirteenth--century polyphony can be unequivocally attributed to composer
figures. This study will take the latter route and will not deal with composers
or authorial influences, and will attempt to reframe the discussion as a study
of cultural phenomena.\par

``Notre Dame school'' is also terminology with some heavy historical
considerations. We know that polyphony of the manner described by Anonymous IV
was performed at Notre Dame of Paris in the twelfth and thirteenth centuries,
but no known manuscript (hereafter MS) is related to that
institution~\autocite{baltzer87}, and Notre Dame is only one institution of many
that are linked to the performance of this style of polyphony. \Leonin{} and
\Perotin{} however, are linked to Notre Dame by their presumably Parisian
identities. What we do know is that this repertory was active, in some form or
another, at various liturgical centres across Europe. It cannot be said
therefore that this is an exclusively ``Notre Dame school''. Rather it is
a repertory with some strong links to Notre Dame. As such, and in order to
distance ourselves from the geographical and repertorial constraints that Notre
Dame enforces upon the repertory, the entire practice will be referred to as the
ND repertory and ND polyphony throughout this study.\par

Of the three central extant sources of the ND repertory (\F{}, \Wone{} and
\Wtwo{}), the MS that has been most puzzling to those that study ND polyphony
--- and as a result by far the most discussed --- is \Wone{}\@. The other two
main MSS are French in origin, perhaps even Parisian, but \Wone{} has now been
determined to be Scottish, most likely from St~Andrews in the early to mid
thirteenth century.\par

\begin{figure}[p]
	\includegraphics[keepaspectratio,width=\linewidth,height=\dimexpr\textheight-2\baselineskip]{f64}
	\caption{\Wone{} f.64, showing the \textit{ex libris} and three--voice music on two--voice staves}\label{fig:f64} \end{figure}

The Scottish provenance of \Wone{}, far outside the usual continental sphere of
the ND repertory, has become the greatest sticking point in the discussion of
this MS\@. Perhaps the most important piece of evidence pointing towards
St~Andrews is the seemingly unequivocal \textit{ex libris} ``Liber monasterii
Sanctii andree apostoli in scotia'' (f.64, see \autoref{fig:f64}). Although this
writing seems to clearly link \Wone{} with St~Andrews, \textcite[7]{ludwig70},
not conceiving that the MS could have originated outside of France, noted that
this could be evidence of a Scottish owner at some point in its history.
\textcite{flotzinger69} cast doubt on the \textit{ex libris} indicating
a Scottish provenance of \Wone{}, noting that it was added later to the MS
neither at the beginning nor the end, but on an inner folio in a different hand
and ink. With regard to the other main piece of evidence --- two responsories
for St~Andrew that appear nowhere else in the repertory ---
\textcite[35]{husmann69} pointed out that `both the South (Winchester) and East
(Ely) of England have the same liturgical arrangement of the St~Andrews
responsories as \Wone{} presupposes',\footnote{``So zeigt sich, da{\ss} sowohl
der S\"uden Englands (Winchester) wie die Osten (Ely) dieselbe liturgische
Anordnung der Andreas-Responsorien besitzen wie die Handschrift \Wone{} sie
voraussetzt''.} indicating that the presence of these responsories does not rule
out a provenance from these other insular institutions.\par

\textcite{roesner74}, in the first full--length study of \Wone{}, dispensed with
these claims, arguing fully for a St~Andrews provenance. He re-evaluated the
\textit{ex libris}, arguing that its position in the MS alone is hardly enough
evidence to disregard it, and it might instead be `an internal mark of ownership
to guard against theft'~\autocite[83]{roesner74}. A perhaps more convincing
argument is \citeauthor{roesner74}'s noticing that the inscription may have been
present before the copying of the work that currently fills the page --- but
likely after the staves had been ruled --- written as it is between the systems
of a page laid out for two--voice music but eventually filled with three--voice
music. However, this theory relies on \Wone{} being dated sometime in the later
fourteenth century in order to be written after the fourteenth--century
\textit{ex libris}, a date suggested by \textcite{roesner74} that now seems much
later than is likely.\par

Providing palaeographical evidence linking the initials and handwriting of
\Wone{} to northern British scribal activity of the early fourteenth century,
\citeauthor{roesner74} claimed that \Wone{}, although not perhaps written at
St~Andrews, was `prepared for that institution'~\autocite[94]{roesner74}. He
argued that the numerous marginalia and jottings present within the MS,
especially the beginnings of letters, provide evidence that `it must have spent
some time in a scriptorium'~\autocite[88]{roesner74}. This evidence was
supplemented in \textcite{brown81}, who provided evidence from the handwriting,
flourished initials and repertory of \Wone{} that it was likely copied at
St~Andrews. Furthermore, they attempted to link the creation of the MS with
David Bernham, Bishop of St~Andrews from 1240--1253, and suggested that it may
have been Bernham or one of his familia that brought the exemplars from which
\Wone{} was copied from Paris to Scotland~\autocite[59--66]{brown81}.\par

\textcite{everist90} argued for an even earlier date, positioning the creation
of \Wone{} in St~Andrews as early as the 1230s, and linking the gathering of the
exemplars from Paris not to Bernham, but to his predecessor William Malveisin,
who began his episcopate as Bishop of St~Andrews in 1202. \citeauthor{everist90}
posited that Malveisin could have brought the exemplars to Scotland on his trip
from Lyon in 1200, which would have necessarily passed through Paris.
\citeauthor{everist90} argued that Malveisin, having heard the music, perhaps
for the first time, in Paris at Christmastime in 1200, could have obtained the
exemplars `more of less off--the--peg from suppliers in Paris' that could have
then been copied at St~Andrews `to form the nucleus of
\Wone{}'~\autocite[28]{everist90}. \textcite{baltzer08} broadly supports this
early dating through analysis of the initials of \Wone{} and close comparison
with other insular MSS of the thirteenth century.\par

However, this palaeographical evidence as concrete and exact proof of a 1230s
date for \Wone{} has recently been called into question by
\textcite[12--22]{bull17}, who argues that many of the examples used by
\citeauthor{everist90} and \citeauthor{baltzer08} of other insular MSS contain
markedly different initials, convincingly arguing that `specific decades do not
necessarily dictate how manuscripts' initials look'~\autocite[17]{bull17}.
A 1230s date for \Wone{} may be the most likely answer to the question of the
period of its copying, but this should not be to the exclusion of all other
possibilities. We should not attempt to pin down a dating to a specific year
from palaeographical evidence alone, as such proof cannot be precise. Rather we
should allow the net to be cast a little wider and use this manner of evidence
to narrow down the possibilities, only eliminating a possible time and place
when the palaeographical evidence can roundly disprove it. There is nothing in
the initials, handwriting or repertory of \Wone{} that could not have also been
true of a MS written decades before or after the 1230s: such scribal style does
not change overnight, but is affected by other factors such as the location,
culture, purpose and fashion of the places where it was written, written for,
and kept at.\par

Nearly every refinement of the dating of \Wone{} has a purpose: to ascertain how
the exemplar quires for the copying of \Wone{} made their way to Scotland,
knowing as we know now that \Wone{} was not likely to have been copied on the
continent. Indeed, it is this question that is contained in the titles of
\textcite{roesner76}, \textcite{brown81}, \textcite{everist90} and
\textcite{baltzer08}. Most of what is written on the palaeography and codicology
of \Wone{} is written to determine the \textit{origin} of \Wone{}, attempting to
pinpoint the exact moment that it came into being, such that it can be linked to
a person who provided the exemplars, usually direct from Paris, for its copying:
Bernham, Malveisin, or some other figure.\par

Intertwined here are three different processes: the creation of the music of
\Wone{}, the performance of this music, and its transmission. These are three
distinct but related processes, and when discussing ND repertory at St~Andrews,
they do not all have to link directly to the ``origin'' of \Wone{}\@. Often
these three processes are conflated, such that the ``origin'' of \Wone{} is
discussed as if the writing of the MS heralded the beginning of a new
performance practice with the arrival of the ND repertory in Scotland. This
thesis will argue that the writing of \Wone{} is not necessarily linked to the
transmission of ND repertory, and a date of \Wone{} can only be used as
a \textit{terminus ante quem} for ND polyphony in Scotland.\par

\citeauthor{roesner74} mentions briefly, when describing the five exemplars that
he detects in the copying of \Wone{}, that he uses the word ``exemplar'' `to
mean ``source complex'' and ``tradition'' as well as
``MS'''~\autocite[81]{roesner74}. This is hinting at an idea that
\citeauthor{roesner74} often mentioned in passing but never tackled in detail:
that each ``exemplar'' he detects is not necessarily a single MS but could be
a set of MSS or indeed an oral tradition. \textcite[27--28]{everist90} does not
believe that it is likely `that one of Mauvoisin's [Malveisin's]
\textit{familia} memorized such an enormous corpus of music in Paris', and it is
an idea `with little appeal'. However, \citeauthor{everist90} simplifies the
process of oral transmission so that it makes little sense, envisaging it as
a direct memorisation of a written source followed by a copying from memory in
St~Andrews, rather like soaking up water with a sponge to deposit elsewhere. The
reality is likely to be far more complex and the medieval methods for absorbing
music, not simply memorising it, will be discussed in \autoref{chap:memory}. We
know, from fragments of polyphony surviving from long--lost polyphonic MS, that
the ND repertory was more common in Britain than the sole surviving complete
source would have us believe. The ND repertory did not necessarily travel
directly from Paris to St~Andrews and could have arrived from some other
liturgical centre.\par

\textcite{baltzer08} hints at this, maintaining that \Wone{} is not simply
a copy of an exemplar or set of exemplars. In her mind, the exemplars were
edited heavily as they were copied into \Wone{}\@. The `less precise' rhythmic
notations of the exemplars were edited such that `their solutions had some local
and personal idiosyncrasies'~\autocite[118]{baltzer08}. In this, Baltzer
imagines a complex series of mixed--up quires that formed the exemplars for
\Wone{}, hastily copied in Paris in completely different dimensions and in an
imprecise notation, such that \Wone{}'s scribes struggled to comprehend and
regularise them for fair copy. In this theory, the writing of \Wone{} was a poor
affair, and the scribes were trying to make the best of a bad situation.
However, this can all be explained more simply by the process of writing--down
what had never been written down before, i.e.\ the transformation of an oral
into a written practice. These issues that \citeauthor{baltzer08} detects can
form evidence to support the ND repertory as an oral tradition. How this can
occur will be discussed in \autoref{chap:formulae}, and examples of orality in
\Wone{} will be analysed in detail in \autoref{chap:analysis}. Put simply, the
possibility that the ``exemplars'' for \Wone{} may not have been physical MSS
should be taken seriously.\par

Of the two foliations of \Wone{}, the older ink foliation that begins in Roman
numerals and then swaps to Arabic numerals at f.30 was added no earlier than the
early fourteenth century. It is clear therefore that by the time the foliation
was added, the MS contained all that it does now in this same order, as well as
some missing material that is implied by gaps in the older foliation, such as
a few missing bifolios and gathering 23.\footnote{This can be seen in the
collation diagram in \textcite[33--39]{staehelin95}.} What we can take from this
is that the writing and collation of \Wone{} are likely not to have occurred
simultaneously if we believe that the writing of \Wone{} was begun as early as
the 1230s. There may have therefore been a not insignificant span of time when
the MS's fascicles were not bound in the order they are now, perhaps only being
bound together and foliated once the repertory was redundant or considered
``complete''.\par

Although \textcite[26]{roesner74} argues that the MS is soiled and stained from
`heavy use', and it is clear that \Wone{} has not been perfectly preserved, it
was neither so heavily used that it ever became unusable: for a MS supposedly
intended `for practical use', it is remarkably well preserved and legible
throughout. The exception to this is the final page (f.214v) which has either
become so scuffed and scratched from contact with an abrasive surface that it
has become illegible, or has been ruined by being used as a pastedown for
a previous binding.\par

In comparison to \F{} or \Wtwo{}, MSS that have a much cleaner and neater layout
with more impressive illumination and more careful binding as singular MSS,
\Wone{} cannot have been a grand testament to an old and stable tradition, but
nor can it have been used daily for any serious period of time. \Wone{} appears
to have been consulted seldom or at the most semi-regularly, never seeing daily
use. It seems likely that \Wone{} was not written for the same purpose as \F{}
or \Wtwo{} for the preservation of a repertory in writing, but as a useful and
perhaps didactic repository of polyphony for St~Andrews. The words ``repertory''
and ``repository'' often bear the same meaning, but in modern usage it may be
best to use both and define \Wone{} as a repertory--repository: a written
repository of an oral repertory.\par

In relation to the order and organisation of \Wone{}, \textcite[111]{baltzer08}
postulates that the illuminator of \Wone{} had `never seen a manuscript of
polyphony before', as such items were presumably fairly rare in the twelfth and
thirteenth centuries. Alternatively, \textcite[28]{everist90} argues that
`whoever assembled the music in W1 had seen such books as F, understood their
structure, and imitated it, or that the exemplar for W1 was already organized
that way and not as a collection of individual quires'. If we accept
\citeauthor{roesner74}'s assertation that \Wone{} was prepared from multiple
``exemplars'', then \citeauthor{everist90}'s latter explanation cannot be
correct, and the scribes of \Wone{} must have been aware of other polyphonic
MSS.\par

On the other hand, perhaps it is not so odd that \Wone{} and books like \F{} are
organised in the same way. For example, \Wtwo{}'s cycle of organa is organised
similarly to \Wone{} and \F{}\@. Rather than come to the conclusion that this
similarity must link all three together, it might be more sensible to say that
organising a collection of music in descending number of parts in liturgical
order was a common way to organise books of polyphony in the thirteenth century,
just like graduals and antiphonals. Moreover, \Wone{} is not simply and strictly
organised from grander to simpler music: fascicle VII breaks this order and
returns to three--voice music after a long passage of two--voice music. Fascicle
VIII begins with one two--voice piece but then continues with three--voice
music, and fascicle IX mixes three-- and two--voice conductus with seeming
freedom. Similarly, fascicles V and VI freely mix music for the Office and for
the Mass, in multiple cycles.\par

Furthermore, it is easy to assume, given the paucity of surviving
thirteenth--century polyphony, that polyphonic MSS were rare items at that time.
However, the notation of MSS like \Wone{} cannot be intuited from a basic
knowledge of writing chant: the complex ligation structures and alignments of
voices require a tradition of writing such music to learn from and, as
\textcite{baltzer08} infers, \Wone{} was not a MS that was simply copied
``dumb'' without any knowledge of the content being copied. We must assume
therefore that the scribes of \Wone{} already knew how to write polyphony and
were skilled at it. Indeed, the notation of polyphonic music in this style may
have been an atypical, but not too extraordinary job for these scribes.\par

\Wone{} therefore cannot have been the beginning of written polyphonic music at
St~Andrews and, by extension, cannot have been the beginning of a performance
practice either as the scribes already knew how this music operated. We may find
more use in thinking of \Wone{} as a MS for the continuation of an
already--present musical practice, an \textit{aide memoire} for novice and
veteran singers alike, to be consulted when their memories fail. The search for
the person who ``brought'' the music to Scotland from Paris may be a red herring
and Malveisin, or whomever we believe instigated the writing of \Wone{}, may
have arrived at St~Andrews to find ND polyphony already there and the writing of
\Wone{} perhaps already in progress. One can speculate that this may not have
surprised Malveisin as much as it may surprise us, for he may have heard this
music in use in the services of nearly all the large religious centres he passed
through on his journey to Scotland. The ND repertory may have been more routine
in Europe than previously thought, and this may be due to an effective oral
tradition.\par
